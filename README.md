# PR1-Altklausuren

Dieses Repository enthält Lösungen für Altklausursaufgaben mit allen zusätzlichen Funktionen.

Beachten Sie, dass verschiedene Methoden auf einfachere Weise implementiert werden können, indem die gesamte C++ - STL genutzt wird. Da in der Prüfung jedoch nur die angegebenen Header-Dateien verwendet werden können, werden alle Lösungen mithilfe der folgenden Header erstellt:

```
#include <iostream>
#include <string>
#include <vector>
```
